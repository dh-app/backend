package com.testdhdev.services.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Jhonatan Candia Romero
 */
@Configuration
@EnableSwagger2
public class Swagger {
    @Bean
    public Docket doctorApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("doctor-services")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.testdhdev.services.controller.doctor"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiEndPointsInfo())
                .useDefaultResponseMessages(false);
    }

    @Bean
    public Docket hospitalApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("hospital-services")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.testdhdev.services.controller.hospital"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiEndPointsInfo())
                .useDefaultResponseMessages(false);
    }

    @Bean
    public Docket noteApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("note-services")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.testdhdev.services.controller.note"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiEndPointsInfo())
                .useDefaultResponseMessages(false);
    }

    @Bean
    public Docket patientApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("patient-services")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.testdhdev.services.controller.patient"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiEndPointsInfo())
                .useDefaultResponseMessages(false);
    }

    @Bean
    public Docket specialityApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("speciality-services")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.testdhdev.services.controller.speciality"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiEndPointsInfo())
                .useDefaultResponseMessages(false);
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder()
                .title("Services API")
                .description("App")
                .contact(new Contact("Jhonatan", "", "tar-jhonatan@hotmail.com"))
                .version("0.0.2")
                .license("Apache 1.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .build();
    }
}
