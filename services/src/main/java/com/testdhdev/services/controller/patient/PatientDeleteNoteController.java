package com.testdhdev.services.controller.patient;

import com.testdhdev.services.command.patient.PatientRemoveNoteCommand;
import com.testdhdev.services.dto.PPatientDTO;
import com.testdhdev.services.input.patient.PatientRemoveNoteInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/patients")
@Api(
        tags = "Delete note",
        description = "Delete a note from a patient"
)
public class PatientDeleteNoteController {
    @Autowired
    private PatientRemoveNoteCommand patientRemoveNoteCommand;

    @ApiOperation(
            value = "This endpoint is to delete a note from a patient, using patientId and noteId"
    )
    @PutMapping("/remove-note")
    public PPatientDTO removeNote(@RequestBody PatientRemoveNoteInput removeNoteInput){
        patientRemoveNoteCommand.setPatientRemoveNoteInput(removeNoteInput);
        patientRemoveNoteCommand.execute();
        PPatientDTO patientDTO = new PPatientDTO(patientRemoveNoteCommand.getPatient());

        return patientDTO;
    }
}
