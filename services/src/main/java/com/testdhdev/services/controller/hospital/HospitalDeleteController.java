package com.testdhdev.services.controller.hospital;

import com.testdhdev.services.command.hospital.HospitalDeleteCommand;
import com.testdhdev.services.dto.HospitalDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/hospitals")
@Api(
        tags = "Delete hospital",
        description = "Delete a hospital"
)
public class HospitalDeleteController {
    @Autowired
    private HospitalDeleteCommand deleteCommand;

    @ApiOperation(
            value = "This endpoint is to delete a hospital using hospital id"
    )
    @DeleteMapping
    public List<HospitalDTO> deleteHospital(@RequestParam("hospitalId") Long hospitalId){
        deleteCommand.setHospitalId(hospitalId);
        deleteCommand.execute();
        List<HospitalDTO> hospitalDTO = deleteCommand.getHospitals()
                .stream()
                .map(hospital -> new HospitalDTO(hospital))
                .collect(Collectors.toList());

        return hospitalDTO;
    }
}
