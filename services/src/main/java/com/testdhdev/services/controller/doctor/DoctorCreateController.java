package com.testdhdev.services.controller.doctor;

import com.testdhdev.services.command.doctor.DoctorCreateCommand;
import com.testdhdev.services.dto.DDoctorDTO;
import com.testdhdev.services.input.doctor.DoctorCreateInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/doctors")
@Api(
        tags = "Register doctor",
        description = "Add a doctor"
)
public class DoctorCreateController {
    @Autowired
    private DoctorCreateCommand doctorCreateCommand;

    @ApiOperation(
            value = "This endpoint is to register a doctor with one o more specialities"
    )
    @PostMapping
    public DDoctorDTO saveDoctor(@RequestBody DoctorCreateInput doctorCreateInput){
        doctorCreateCommand.setDoctorCreateInput(doctorCreateInput);
        doctorCreateCommand.execute();
        DDoctorDTO DDoctorDTO = new DDoctorDTO(doctorCreateCommand.getDoctor());

        return DDoctorDTO;
    }
}
