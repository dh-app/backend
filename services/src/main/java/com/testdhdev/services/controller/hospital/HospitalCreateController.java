package com.testdhdev.services.controller.hospital;

import com.testdhdev.services.command.hospital.HospitalCreateCommand;
import com.testdhdev.services.dto.HospitalDTO;
import com.testdhdev.services.input.hospital.HospitalCreateInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/hospitals")
@Api(
        tags = "Register hospital",
        description = "Add a hospital"
)
public class HospitalCreateController {
    @Autowired
    private HospitalCreateCommand hospitalCreateCommand;

    @ApiOperation(
            value = "This endpoint is to register a hospital with one o more patients and doctors"
    )
    @PostMapping
    public HospitalDTO saveHospital(@RequestBody HospitalCreateInput hospitalCreateInput){
        hospitalCreateCommand.setCreateInput(hospitalCreateInput);
        hospitalCreateCommand.execute();
        HospitalDTO hospitalDTO = new HospitalDTO(hospitalCreateCommand.getHospital());

        return hospitalDTO;
    }
}
