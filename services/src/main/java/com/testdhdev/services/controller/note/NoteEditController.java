package com.testdhdev.services.controller.note;

import com.testdhdev.services.command.note.NoteEditCommand;
import com.testdhdev.services.input.note.NoteEditInput;
import com.testdhdev.services.model.domain.Note;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/notes")
@Api(
        tags = "Edit notes",
        description = "Change values to a note"
)
public class NoteEditController {
    @Autowired
    private NoteEditCommand noteEditCommand;

    @ApiOperation(
            value = "This endpoint is to change the data of a note, using noteId with new values"
    )
    @PutMapping
    public Note editNote(@RequestBody NoteEditInput editInput){
        noteEditCommand.setNoteEditInput(editInput);
        noteEditCommand.execute();

        return noteEditCommand.getNote();
    }
}
