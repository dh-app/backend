package com.testdhdev.services.controller.speciality;

import com.testdhdev.services.command.speciality.SpecialityEditCommand;
import com.testdhdev.services.input.speciality.SpecialityEditInput;
import com.testdhdev.services.model.domain.Speciality;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/specialities")
@Api(
        tags = "Edit specialities",
        description = "Change values to a speciality"
)
public class SpecialityEditController {
    @Autowired
    private SpecialityEditCommand editCommand;

    @ApiOperation(
            value = "This endpoint is to change the data of a speciality, using specialityId with new values"
    )
    @PutMapping
    public Speciality editSpeciality(@RequestBody SpecialityEditInput editInput){
        editCommand.setEditInput(editInput);
        editCommand.execute();

        return editCommand.getSpeciality();
    }
}
