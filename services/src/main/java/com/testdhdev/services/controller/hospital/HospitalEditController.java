package com.testdhdev.services.controller.hospital;

import com.testdhdev.services.command.hospital.HospitalEditCommand;
import com.testdhdev.services.dto.HospitalDTO;
import com.testdhdev.services.input.hospital.HospitalEditInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/hospitals")
@Api(
        tags = "Edit hospital",
        description = "Change values to a hospital"
)
public class HospitalEditController {
    @Autowired
    private HospitalEditCommand editCommand;

    @ApiOperation(
            value = "This endpoint is to change the data of a hospital, using hospitalId with new values"
    )
    @PutMapping
    public HospitalDTO editHospital(@RequestBody HospitalEditInput editInput){
        editCommand.setHospitalEditInput(editInput);
        editCommand.execute();
        HospitalDTO hospitalDTO = new HospitalDTO(editCommand.getHospital());

        return hospitalDTO;
    }
}
