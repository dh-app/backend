package com.testdhdev.services.controller.patient;

import com.testdhdev.services.command.patient.PatientDeleteCommand;
import com.testdhdev.services.dto.PPatientDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/patients")
@Api(
        tags = "Delete patient",
        description = "Delete a patient"
)
public class PatientDeleteController {
    @Autowired
    private PatientDeleteCommand deleteCommand;

    @ApiOperation(
            value = "This endpoint is to delete a patient using patientId"
    )
    @DeleteMapping
    public List<PPatientDTO> deletePatient(@RequestParam("patientId") Long patientId){
        deleteCommand.setPatientId(patientId);
        deleteCommand.execute();
        List<PPatientDTO> patientDTOS = deleteCommand.getPatients()
                .stream()
                .map(patient -> new PPatientDTO(patient)).collect(Collectors.toList());

        return patientDTOS;
    }
}
