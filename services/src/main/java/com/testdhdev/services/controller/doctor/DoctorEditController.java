package com.testdhdev.services.controller.doctor;

import com.testdhdev.services.command.doctor.DoctorEditCommand;
import com.testdhdev.services.dto.DDoctorDTO;
import com.testdhdev.services.input.doctor.DoctorEditInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/doctors")
@Api(
        tags = "Edit doctor",
        description = "Change values to a doctor"
)
public class DoctorEditController {
    @Autowired
    private DoctorEditCommand editCommand;

    @ApiOperation(
            value = "This endpoint is to change the data of a doctor, using doctor id with new values"
    )
    @PutMapping
    public DDoctorDTO editDoctor(@RequestBody DoctorEditInput editInput){
        editCommand.setDoctorEditInput(editInput);
        editCommand.execute();
        DDoctorDTO DDoctorDTO = new DDoctorDTO(editCommand.getDoctor());

        return DDoctorDTO;
    }
}

