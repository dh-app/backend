package com.testdhdev.services.controller.hospital;

import com.testdhdev.services.command.hospital.HospitalListCommand;
import com.testdhdev.services.dto.HospitalDTO;
import com.testdhdev.services.model.domain.Hospital;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/hospitals")
@Api(
        tags = "List hospitals",
        description = "Get list of hospitals"
)
public class HospitalListController {
    @Autowired
    private HospitalListCommand hospitalListCommand;

    @ApiOperation(
            value = "This endpoint return the list of hospitals"
    )
    @GetMapping
    public List<HospitalDTO> hospitalList(){
        hospitalListCommand.execute();
        List<HospitalDTO> list = hospitalListCommand.getHospitals()
                .stream()
                .map(hospital -> new HospitalDTO(hospital))
                .collect(Collectors.toList());

        return list;
    }
}
