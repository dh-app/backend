package com.testdhdev.services.controller.speciality;

import com.testdhdev.services.command.speciality.SpecialityListCommand;
import com.testdhdev.services.model.domain.Speciality;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/specialities")
public class SpecialityListController {
    @Autowired
    private SpecialityListCommand listCommand;

    @GetMapping
    public List<Speciality> getSpecialities(){
        listCommand.execute();

        return listCommand.getSpecialities();
    }
}
