package com.testdhdev.services.controller.hospital;

import com.testdhdev.services.command.hospital.HospitalDeletePatientCommand;
import com.testdhdev.services.dto.HospitalDTO;
import com.testdhdev.services.input.hospital.HospitalDeletePatientInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/hospitals")
@Api(
        tags = "Delete patient",
        description = "Delete a patient from a hospital"
)
public class HospitalDeletePatientController {
    @Autowired
    private HospitalDeletePatientCommand deletePatientCommand;

    @ApiOperation(
            value = "This endpoint is to delete a patient from a hospital, using patientId and hospitalId"
    )
    @DeleteMapping("/remove-patient")
    public HospitalDTO deletePatient(@RequestBody HospitalDeletePatientInput patientInput){
        deletePatientCommand.setPatientInput(patientInput);
        deletePatientCommand.execute();
        HospitalDTO hospitalDTO = new HospitalDTO(deletePatientCommand.getHospital());

        return hospitalDTO;
    }
}
