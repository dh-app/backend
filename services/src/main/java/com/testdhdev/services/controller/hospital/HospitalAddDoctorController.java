package com.testdhdev.services.controller.hospital;

import com.testdhdev.services.command.hospital.HospitalAddDoctorCommand;
import com.testdhdev.services.dto.HospitalDTO;
import com.testdhdev.services.input.hospital.HospitalAddDoctorInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/hospitals")
@Api(
        tags = "Doctor to hospital",
        description = "Add doctors to hospital"
)
public class HospitalAddDoctorController {
    @Autowired
    private HospitalAddDoctorCommand addDoctorCommand;

    @ApiOperation(
            value = "This endpoint is to assign one o more doctors to a hospital"
    )
    @PutMapping("/add-doctor")
    public HospitalDTO addDoctor(@RequestBody HospitalAddDoctorInput input){
        addDoctorCommand.setInput(input);
        addDoctorCommand.execute();
        HospitalDTO hospitalDTO = new HospitalDTO(addDoctorCommand.getHospital());

        return hospitalDTO;
    }
}
