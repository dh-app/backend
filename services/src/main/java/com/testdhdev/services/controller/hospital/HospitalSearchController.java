package com.testdhdev.services.controller.hospital;

import com.testdhdev.services.command.hospital.HospitalSearchCommand;
import com.testdhdev.services.commons.Pagination;
import com.testdhdev.services.dto.HospitalDTO;
import com.testdhdev.services.model.domain.Hospital;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.stream.Collectors;


/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/hospitals")
@Api(
        tags = "Search hospitals",
        description = "Search hospitals using name"
)
public class HospitalSearchController {
    @Autowired
    private HospitalSearchCommand searchByNameCommand;

    @ApiOperation(
            value = "This endpoint return the list of hospitals paginated"
    )
    @GetMapping("/search")
    public Pagination<HospitalDTO> searchHospitalByName(@RequestParam Integer limit,
                                                        @RequestParam Integer page,
                                                        @RequestParam(value = "name", required = false) String name){
        searchByNameCommand.setLimit(limit);
        searchByNameCommand.setPage(page);
        searchByNameCommand.setName(name);
        searchByNameCommand.execute();

        Pagination<HospitalDTO> pagination = new Pagination<>();
        pagination.setContent(searchByNameCommand.getHospitals()
                .stream()
                .map(hospital -> new HospitalDTO(hospital)).collect(Collectors.toList()));
        pagination.setTotalElements(searchByNameCommand.getTotalElements());
        pagination.setTotalPages(searchByNameCommand.getTotalPages());

        return pagination;
    }
}
