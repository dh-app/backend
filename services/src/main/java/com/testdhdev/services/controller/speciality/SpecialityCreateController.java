package com.testdhdev.services.controller.speciality;

import com.testdhdev.services.command.speciality.SpecialityCreateCommand;
import com.testdhdev.services.input.speciality.SpecialityCreateInput;
import com.testdhdev.services.model.domain.Speciality;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/specialities")
@Api(
        tags = "Register speciality",
        description = "Add a speciality"
)
public class SpecialityCreateController {
    @Autowired
    private SpecialityCreateCommand createCommand;

    @ApiOperation(
            value = "This endpoint is to register a speciality"
    )
    @PostMapping
    public Speciality saveSpeciality(@RequestBody SpecialityCreateInput input){
        createCommand.setCreateInput(input);
        createCommand.execute();

        return createCommand.getSpeciality();
    }
}
