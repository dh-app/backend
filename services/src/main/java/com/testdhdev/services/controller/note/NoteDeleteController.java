package com.testdhdev.services.controller.note;

import com.testdhdev.services.command.note.NoteDeleteCommand;
import com.testdhdev.services.model.domain.Note;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/notes")
@Api(
        tags = "Delete note",
        description = "Delete a note"
)
public class NoteDeleteController {
    @Autowired
    private NoteDeleteCommand deleteCommand;

    @ApiOperation(
            value = "This endpoint is to delete a note"
    )
    @DeleteMapping
    public List<Note> deleteNote(@RequestParam("noteId") Long noteId){
        deleteCommand.setNoteId(noteId);
        deleteCommand.execute();

        return deleteCommand.getNotes();
    }
}
