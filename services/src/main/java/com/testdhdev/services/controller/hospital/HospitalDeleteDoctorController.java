package com.testdhdev.services.controller.hospital;

import com.testdhdev.services.command.hospital.HospitalDeleteDoctorCommand;
import com.testdhdev.services.dto.HospitalDTO;
import com.testdhdev.services.input.hospital.HospitalDeleteDoctorInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/hospitals")
@Api(
        tags = "Delete doctor",
        description = "Delete a doctor from a hospital"
)
public class HospitalDeleteDoctorController {
    @Autowired
    private HospitalDeleteDoctorCommand deleteDoctorCommand;

    @ApiOperation(
            value = "This endpoint is to delete a doctor from a hospital, using hospitalId and doctorId"
    )
    @DeleteMapping("/remove-doctor")
    public HospitalDTO deleteDoctorHospital(@RequestBody HospitalDeleteDoctorInput input){
        deleteDoctorCommand.setDeleteDoctorInput(input);
        deleteDoctorCommand.execute();
        HospitalDTO hospitalDTO = new HospitalDTO(deleteDoctorCommand.getHospital());

        return hospitalDTO;
    }
}
