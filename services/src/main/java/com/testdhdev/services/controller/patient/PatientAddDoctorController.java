package com.testdhdev.services.controller.patient;

import com.testdhdev.services.command.patient.PatientAddDoctorCommand;
import com.testdhdev.services.dto.PPatientDTO;
import com.testdhdev.services.input.patient.PatientAddDoctorsInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/patients")
public class PatientAddDoctorController {
    @Autowired
    private PatientAddDoctorCommand addDoctorCommand;

    @PutMapping("/add-doctor")
    public PPatientDTO addDoctorToPatient(@RequestBody PatientAddDoctorsInput addDoctorsInput){
        addDoctorCommand.setPatientAddDoctorsInput(addDoctorsInput);
        addDoctorCommand.execute();
        PPatientDTO pPatientDTO = new PPatientDTO(addDoctorCommand.getPatient());

        return pPatientDTO;
    }
}
