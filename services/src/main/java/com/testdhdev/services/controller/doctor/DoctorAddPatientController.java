package com.testdhdev.services.controller.doctor;

import com.testdhdev.services.command.doctor.DoctorAddPatientCommand;
import com.testdhdev.services.dto.DDoctorDTO;
import com.testdhdev.services.input.doctor.DoctorAddPatientInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/doctors")
public class DoctorAddPatientController {
    @Autowired
    private DoctorAddPatientCommand patientCommand;

    @PutMapping("/add-patients")
    public DDoctorDTO addPatient(@RequestBody DoctorAddPatientInput input){
        patientCommand.setAddPatientInput(input);
        patientCommand.execute();
        DDoctorDTO DDoctorDTO = new DDoctorDTO(patientCommand.getDoctor());

        return DDoctorDTO;
    }
}
