package com.testdhdev.services.controller.note;

import com.testdhdev.services.command.note.NoteCreateCommand;
import com.testdhdev.services.input.note.NoteCreateInput;
import com.testdhdev.services.model.domain.Note;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/notes")
@Api(
        tags = "Register note",
        description = "Add a note"
)
public class NoteCreateController {
    @Autowired
    private NoteCreateCommand noteCreateCommand;

    @ApiOperation(
            value = "This endpoint is to register a note"
    )
    @PostMapping
    public Note saveNote(@RequestBody NoteCreateInput noteCreateInput){
        noteCreateCommand.setNoteCreateInput(noteCreateInput);
        noteCreateCommand.execute();

        return noteCreateCommand.getNote();
    }
}
