package com.testdhdev.services.controller.patient;

import com.testdhdev.services.command.patient.PatientListCommand;
import com.testdhdev.services.dto.PPatientDTO;
import com.testdhdev.services.model.domain.Patient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/patients")
@Api(
        tags = "List patients",
        description = "Get list of patients"
)
public class PatientListController {
    @Autowired
    private PatientListCommand patientListCommand;

    @ApiOperation(
            value = "This endpoint return the list of patients"
    )
    @GetMapping
    public List<PPatientDTO> getPatients(){
        patientListCommand.execute();
        List<PPatientDTO> patientDTOS = patientListCommand.getPatients()
                .stream()
                .map(patient -> new PPatientDTO(patient)).collect(Collectors.toList());

        return patientDTOS;
    }
}
