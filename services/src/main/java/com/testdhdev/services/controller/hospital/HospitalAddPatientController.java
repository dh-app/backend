package com.testdhdev.services.controller.hospital;

import com.testdhdev.services.command.hospital.HospitalAddPatientCommand;
import com.testdhdev.services.dto.HospitalDTO;
import com.testdhdev.services.input.hospital.HospitalAddPatientInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/hospitals")
@Api(
        tags = "Patient to hospital",
        description = "Add patients to hospital"
)
public class HospitalAddPatientController {
    @Autowired
    private HospitalAddPatientCommand patientCommand;

    @ApiOperation(
            value = "This endpoint is to assign one o more patients to a hospital"
    )
    @PutMapping("/add-patients")
    public HospitalDTO addPatients(@RequestBody HospitalAddPatientInput patientInput){
        patientCommand.setPatientInput(patientInput);
        patientCommand.execute();
        HospitalDTO hospitalDTO = new HospitalDTO(patientCommand.getHospital());

        return hospitalDTO;
    }
}
