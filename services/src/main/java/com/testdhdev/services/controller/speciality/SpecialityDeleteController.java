package com.testdhdev.services.controller.speciality;

import com.testdhdev.services.command.speciality.SpecialityDeleteCommand;
import com.testdhdev.services.model.domain.Speciality;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/specialities")
@Api(
        tags = "Delete specialities",
        description = "Delete a speciality"
)
public class SpecialityDeleteController {
    @Autowired
    private SpecialityDeleteCommand deleteCommand;

    @ApiOperation(
            value = "This endpoint is to delete a speciality"
    )
    @DeleteMapping
    public List<Speciality> deleteSpeciality(@RequestParam("specialityId") Long specialityId){
        deleteCommand.setSpecialityId(specialityId);
        deleteCommand.execute();

        return deleteCommand.getSpecialities();
    }
}
