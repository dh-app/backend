package com.testdhdev.services.controller.doctor;

import com.testdhdev.services.command.doctor.DoctorSearchCommand;
import com.testdhdev.services.commons.Pagination;
import com.testdhdev.services.dto.DDoctorDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.stream.Collectors;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/doctors")
@Api(
        tags = "Search doctors",
        description = "Search doctors using name or lastname"
)
public class DoctorSearchController {

    @Autowired
    private DoctorSearchCommand command;

    @ApiOperation(
            value = "This endpoint return the list of doctors paginated"
    )
    @GetMapping("/search")
    public Pagination<DDoctorDTO> searchDoctorByNameOrLastName(@RequestParam Integer limit,
                                                               @RequestParam Integer page,
                                                               @RequestParam(value = "name", required = false) String name,
                                                               @RequestParam(value = "lastName", required = false) String lastName){
        command.setLimit(limit);
        command.setPage(page);
        command.setName(name);
        command.setLastName(lastName);
        command.execute();

        Pagination<DDoctorDTO> pagination = new Pagination<>();
        pagination.setContent(command.getDoctors()
                .stream()
                .map(doctor -> new DDoctorDTO(doctor)).collect(Collectors.toList()));
        pagination.setTotalElements(command.getTotalElements());
        pagination.setTotalPages(command.getTotalPages());

        return pagination;
    }
}
