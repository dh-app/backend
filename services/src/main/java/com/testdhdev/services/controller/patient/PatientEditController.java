package com.testdhdev.services.controller.patient;

import com.testdhdev.services.command.patient.PatientEditCommand;
import com.testdhdev.services.dto.PPatientDTO;
import com.testdhdev.services.input.patient.PatientEditInput;
import com.testdhdev.services.model.domain.Patient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/patients")
@Api(
        tags = "Edit patient",
        description = "Change values to a patient"
)
public class PatientEditController {

    @Autowired
    private PatientEditCommand editCommand;

    @ApiOperation(
            value = "This endpoint is to change the data of a patient, using patientId with new values"
    )
    @PutMapping
    public PPatientDTO editPatient(@RequestBody PatientEditInput input){
        editCommand.setInput(input);
        editCommand.execute();
        PPatientDTO patientDTO = new PPatientDTO(editCommand.getPatient());

        return patientDTO;
    }
}
