package com.testdhdev.services.controller.patient;

import com.testdhdev.services.command.patient.PatientSearchCommand;
import com.testdhdev.services.commons.Pagination;
import com.testdhdev.services.dto.PPatientDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.stream.Collectors;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/patients")
@Api(
        tags = "Search patients",
        description = "Search patients using name or lastname"
)
public class PatientSearchController {
    @Autowired
    private PatientSearchCommand command;

    @ApiOperation(
            value = "This endpoint return the list of patients paginated"
    )
    @GetMapping("/search")
    public Pagination<PPatientDTO> searchPatientByNameOrLastname(@RequestParam Integer limit,
                                                                 @RequestParam Integer page,
                                                                 @RequestParam(value = "name", required = false) String name,
                                                                 @RequestParam(value = "lastName", required = false) String lastName){
        command.setLimit(limit);
        command.setPage(page);
        command.setLastName(lastName);
        command.setName(name);
        command.execute();

        Pagination<PPatientDTO> pagination = new Pagination<>();
        pagination.setContent(command.getPatients()
                .stream()
                .map(patient -> new PPatientDTO(patient)).collect(Collectors.toList()));
        pagination.setTotalElements(command.getTotalElements());
        pagination.setTotalPages(command.getTotalPages());

        return pagination;
    }
}
