package com.testdhdev.services.controller.patient;

import com.testdhdev.services.command.patient.PatientCreateCommand;
import com.testdhdev.services.dto.PPatientDTO;
import com.testdhdev.services.input.patient.PatientCreateInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/patients")
@Api(
        tags = "Register patient",
        description = "Add a patient"
)
public class PatientCreateController {

    @Autowired
    private PatientCreateCommand patientCreateCommand;

    @ApiOperation(
            value = "This endpoint is to register a patient"
    )
    @PostMapping
    public PPatientDTO savePatient(@RequestBody PatientCreateInput patientCreateInput){
        patientCreateCommand.setPatientInput(patientCreateInput);
        patientCreateCommand.execute();
        PPatientDTO patientDTO = new PPatientDTO(patientCreateCommand.getPatient());

        return patientDTO;
    }
}
