package com.testdhdev.services.controller.doctor;

import com.testdhdev.services.command.doctor.DoctorDeleteCommand;
import com.testdhdev.services.dto.DDoctorDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/doctors")
@Api(
        tags = "Delete doctor",
        description = "Delete a doctor"
)
public class DoctorDeleteController {
    @Autowired
    private DoctorDeleteCommand deleteCommand;

    @ApiOperation(
            value = "This endpoint is to delete a doctor using doctor id"
    )
    @DeleteMapping
    public List<DDoctorDTO> deleteDoctor(@RequestParam("doctorId") Long doctorId){
        deleteCommand.setDoctorId(doctorId);
        deleteCommand.execute();
        List<DDoctorDTO> DDoctorDTOS = deleteCommand.getDoctors()
                .stream()
                .map(doctor -> new DDoctorDTO(doctor)).collect(Collectors.toList());

        return DDoctorDTOS;
    }
}
