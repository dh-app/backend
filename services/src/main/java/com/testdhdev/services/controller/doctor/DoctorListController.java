package com.testdhdev.services.controller.doctor;

import com.testdhdev.services.command.doctor.DoctorListCommand;
import com.testdhdev.services.dto.DDoctorDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/doctors")
@Api(
        tags = "List doctors",
        description = "Get list of doctors"
)
public class DoctorListController {
    @Autowired
    private DoctorListCommand doctorListCommand;

    @ApiOperation(
            value = "This endpoint return the list of doctors"
    )
    @GetMapping
    public List<DDoctorDTO> getDoctors(){
        doctorListCommand.execute();
        List<DDoctorDTO> DDoctorDTOS = doctorListCommand.getDoctors()
                .stream()
                .map(doctor -> new DDoctorDTO(doctor)).collect(Collectors.toList());

        return DDoctorDTOS;
    }

}
