package com.testdhdev.services.controller.doctor;

import com.testdhdev.services.command.doctor.DoctorRemoveSpecialityCommand;
import com.testdhdev.services.dto.DDoctorDTO;
import com.testdhdev.services.input.doctor.DoctorRemoveSpecialityInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/doctors")
@Api(
        tags = "Delete speciality",
        description = "Delete a speciality from a doctor"
)
public class DoctorDeleteSpecialityController {
    @Autowired
    private DoctorRemoveSpecialityCommand doctorRemoveSpecialityCommand;

    @ApiOperation(
            value = "This endpoint is to delete a speciality from a doctor, using doctorId and specialityId"
    )
    @DeleteMapping("/remove-speciality")
    public DDoctorDTO deleteSpeciality(@RequestBody DoctorRemoveSpecialityInput input){
        doctorRemoveSpecialityCommand.setRemoveSpecialityInput(input);
        doctorRemoveSpecialityCommand.execute();
        DDoctorDTO DDoctorDTO = new DDoctorDTO(doctorRemoveSpecialityCommand.getDoctor());

        return DDoctorDTO;
    }
}
