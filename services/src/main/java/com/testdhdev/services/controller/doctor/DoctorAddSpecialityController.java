package com.testdhdev.services.controller.doctor;

import com.testdhdev.services.command.doctor.DoctorAddSpecialityCommand;
import com.testdhdev.services.dto.DDoctorDTO;
import com.testdhdev.services.input.doctor.DoctorAddSpecialityInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/doctors")
@Api(
        tags = "Speciality to doctor",
        description = "Add specialities to doctor"
)
public class DoctorAddSpecialityController {
    @Autowired
    private DoctorAddSpecialityCommand doctorAddSpecialityCommand;

    @ApiOperation(
            value = "This endpoint is to assign one o more specialities to a doctor"
    )
    @PutMapping("/add-speciality")
    public DDoctorDTO addSpeciality(@RequestBody DoctorAddSpecialityInput doctorAddSpecialityInput){
        doctorAddSpecialityCommand.setInput(doctorAddSpecialityInput);
        doctorAddSpecialityCommand.execute();
        DDoctorDTO DDoctorDTO = new DDoctorDTO(doctorAddSpecialityCommand.getDoctor());

        return DDoctorDTO;
    }
}
