package com.testdhdev.services.controller.patient;

import com.testdhdev.services.command.patient.PatientAddNoteCommand;
import com.testdhdev.services.dto.PPatientDTO;
import com.testdhdev.services.input.patient.PatientAddNotesInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestScope
@RequestMapping("/patients")
@Api(
        tags = "Note to patient",
        description = "Add notes to patient"
)
public class PatientAddNoteController {

    @Autowired
    private PatientAddNoteCommand patientAddNoteCommand;

    @ApiOperation(
            value = "This endpoint is to assign one o more notes to a patient"
    )
    @PutMapping("/add-note")
    public PPatientDTO addNotes(@RequestBody PatientAddNotesInput addNotesInput){
        patientAddNoteCommand.setAddNotesInput(addNotesInput);
        patientAddNoteCommand.execute();
        PPatientDTO patientDTO = new PPatientDTO(patientAddNoteCommand.getPatient());

        return patientDTO;
    }
}
