package com.testdhdev.services.command.patient;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.model.domain.Patient;
import com.testdhdev.services.model.repository.PatientRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class PatientListCommand implements BusinessLogicCommand {
    @Autowired
    private PatientRepository patientRepository;

    @Getter
    private List<Patient> patients;

    @Override
    public void execute(){
        patients = patientRepository.findAll();
    }
}
