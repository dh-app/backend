package com.testdhdev.services.command.doctor;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.doctor.DoctorEditInput;
import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.repository.DoctorRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class DoctorEditCommand implements BusinessLogicCommand {
    @Setter
    private DoctorEditInput doctorEditInput;

    @Getter
    private Doctor doctor;

    @Autowired
    private DoctorRepository doctorRepository;

    @Override
    public void execute(){
        doctor = editDoctor(doctorEditInput);
    }

    private Doctor editDoctor(DoctorEditInput editInput){
        Doctor doctor = doctorRepository.findDoctorById(editInput.getDoctorId());
        doctor.setUpdateBy(editInput.getUser());
        doctor.setUpdatedDate(new Date());
        doctor.setProfileImageUrl(editInput.getProfileImageUrl());
        doctor.setAddress(editInput.getAddress());
        doctor.setLastName(editInput.getLastName());
        doctor.setName(editInput.getName());
        doctor.setDateBirth(editInput.getDateBirth());

        return doctorRepository.save(doctor);
    }
}
