package com.testdhdev.services.command.hospital;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.model.domain.Hospital;
import com.testdhdev.services.model.repository.HospitalRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class HospitalDeleteCommand implements BusinessLogicCommand {
    @Setter
    private Long hospitalId;

    @Getter
    private List<Hospital> hospitals;

    @Autowired
    private HospitalRepository hospitalRepository;

    @Override
    public void execute(){
        hospitals = deleteHospital(hospitalId);
    }

    private List<Hospital> deleteHospital(Long hospitalId){
        hospitalRepository.deleteById(hospitalId);

        return hospitalRepository.findAll();
    }
}
