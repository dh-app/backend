package com.testdhdev.services.command.hospital;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.hospital.HospitalAddDoctorInput;
import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.domain.Hospital;
import com.testdhdev.services.model.repository.DoctorRepository;
import com.testdhdev.services.model.repository.HospitalRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class HospitalAddDoctorCommand implements BusinessLogicCommand {
    @Setter
    private HospitalAddDoctorInput input;

    @Getter
    private Hospital hospital;

    @Autowired
    private HospitalRepository repository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Override
    public void execute(){
        hospital = addDoctorHospital(input);
    }

    private Hospital addDoctorHospital(HospitalAddDoctorInput input){
        Hospital hospital = repository.findHospitalById(input.getHospitalId());
        input.getDoctors().forEach(id -> {
            Doctor doctor = doctorRepository.findDoctorById(id);
            hospital.addDoctor(doctor);
        });

        return repository.save(hospital);
    }
}
