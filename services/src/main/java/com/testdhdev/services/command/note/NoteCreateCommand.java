package com.testdhdev.services.command.note;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.note.NoteCreateInput;
import com.testdhdev.services.model.domain.Note;
import com.testdhdev.services.model.repository.NoteRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class NoteCreateCommand implements BusinessLogicCommand {

    @Setter
    private NoteCreateInput noteCreateInput;

    @Getter
    private Note note;

    @Autowired
    private NoteRepository noteRepository;

    @Override
    public void execute(){
        note = saveNote(noteCreateInput);
    }

    private Note saveNote(NoteCreateInput noteCreateInput) {
        Note note = composeNote(noteCreateInput);

        return noteRepository.save(note);
    }

    private Note composeNote(NoteCreateInput noteCreateInput) {
        Note note = new Note();
        note.setCreatedBy(noteCreateInput.getUser());
        note.setUpdateBy(noteCreateInput.getUser());
        note.setCreatedDate(noteCreateInput.getCreatedDate());
        note.setDescription(noteCreateInput.getDescription());
        note.setUpdatedDate(noteCreateInput.getCreatedDate());

        return note;
    }
}
