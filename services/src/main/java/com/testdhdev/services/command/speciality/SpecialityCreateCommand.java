package com.testdhdev.services.command.speciality;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.speciality.SpecialityCreateInput;
import com.testdhdev.services.model.domain.Speciality;
import com.testdhdev.services.model.repository.SpecialityRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class SpecialityCreateCommand implements BusinessLogicCommand {

    @Setter
    private SpecialityCreateInput createInput;

    @Getter
    private Speciality speciality;

    @Autowired
    private SpecialityRepository specialityRepository;

    @Override
    public void execute(){
        speciality = saveSpeciality(createInput);
    }

    private Speciality saveSpeciality(SpecialityCreateInput input){
        Speciality speciality = composeSpeciality(input);

        return specialityRepository.save(speciality);
    }

    private Speciality composeSpeciality(SpecialityCreateInput input){
        Speciality speciality = new Speciality();
        speciality.setCreatedBy(input.getUser());
        speciality.setDescription(input.getDescription());
        speciality.setName(input.getName());
        speciality.setUpdateBy(input.getUser());

        return speciality;
    }
}
