package com.testdhdev.services.command.patient;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.patient.PatientRemoveNoteInput;
import com.testdhdev.services.model.domain.Note;
import com.testdhdev.services.model.domain.Patient;
import com.testdhdev.services.model.repository.NoteRepository;
import com.testdhdev.services.model.repository.PatientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class PatientRemoveNoteCommand implements BusinessLogicCommand {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private NoteRepository noteRepository;

    @Getter
    private Patient patient;

    @Setter
    private PatientRemoveNoteInput patientRemoveNoteInput;

    @Override
    public void execute(){
        patient = removeNote(patientRemoveNoteInput);
    }

    private Patient removeNote(PatientRemoveNoteInput patientRemoveNoteInput){
        Patient patient = patientRepository.findPatientById(patientRemoveNoteInput.getPatientId());
        Note note = noteRepository.findNoteById(patientRemoveNoteInput.getNoteId());
        patient.removeNote(note);

        return patientRepository.save(patient);
    }

}
