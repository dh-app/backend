package com.testdhdev.services.command.patient;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.model.domain.Patient;
import com.testdhdev.services.model.repository.PatientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class PatientDeleteCommand implements BusinessLogicCommand {

    @Setter
    private Long patientId;

    @Getter
    private List<Patient> patients;

    @Autowired
    private PatientRepository patientRepository;

    @Override
    public void execute(){
        patients = deletePatients(patientId);
    }

    private List<Patient> deletePatients(Long id){
        patientRepository.deleteById(id);

        return patientRepository.findAll();
    }
}
