package com.testdhdev.services.command.doctor;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.doctor.DoctorRemoveSpecialityInput;
import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.domain.Speciality;
import com.testdhdev.services.model.repository.DoctorRepository;
import com.testdhdev.services.model.repository.SpecialityRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class DoctorRemoveSpecialityCommand implements BusinessLogicCommand {

    @Setter
    private DoctorRemoveSpecialityInput removeSpecialityInput;

    @Getter
    private Doctor doctor;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private SpecialityRepository specialityRepository;

    @Override
    public void execute(){
        doctor = deleteSpeciality(removeSpecialityInput);
    }

    private Doctor deleteSpeciality(DoctorRemoveSpecialityInput input){
        Doctor doctor = doctorRepository.findDoctorById(input.getDoctorId());
        Speciality speciality = specialityRepository.findSpecialityById(input.getSpecialityId());
        doctor.removeSpeciality(speciality);

        return doctorRepository.save(doctor);
    }
}
