package com.testdhdev.services.command.patient;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.patient.PatientCreateInput;
import com.testdhdev.services.model.domain.Patient;
import com.testdhdev.services.model.repository.PatientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class PatientCreateCommand implements BusinessLogicCommand {
    @Setter
    private PatientCreateInput patientInput;

    @Getter
    private Patient patient;

    @Autowired
    private PatientRepository patientRepository;

    @Override
    public void execute(){
        patient = savePatient(patientInput);
    }

    private Patient savePatient(PatientCreateInput patientCreateInput){
        Patient patient = composePatient(patientCreateInput);

        return patientRepository.save(patient);
    }

    private Patient composePatient(PatientCreateInput input){
        Patient patient = new Patient();
        patient.setUpdateBy(input.getUser());
        patient.setProfileImageUrl(input.getProfileImageUrl());
        patient.setName(input.getName());
        patient.setLastName(input.getLastName());
        patient.setAddress(input.getAddress());
        patient.setDateBirth(input.getDateBirth());
        patient.setCreatedBy(input.getUser());

        return patient;
    }
}
