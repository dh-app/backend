package com.testdhdev.services.command.doctor;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.domain.Doctor_;
import com.testdhdev.services.model.repository.DoctorRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class DoctorSearchCommand implements BusinessLogicCommand {
    @Setter
    private Integer limit;

    @Setter
    private Integer page;

    @Setter
    private String name;

    @Setter
    private String lastName;

    @Getter
    private List<Doctor> doctors;

    @Getter
    private Long totalElements;

    @Getter
    private Integer totalPages;

    @Autowired
    private DoctorRepository doctorRepository;

    @Override
    public void execute(){
        doctors = new ArrayList<>();

        PageRequest pageRequest = PageRequest.of(page, limit);
        Page<Doctor> pageResult = doctorRepository.findAll(buildSpecification(), pageRequest);
        List<Doctor> content = pageResult.getContent();

        if (!CollectionUtils.isEmpty(content)) doctors.addAll(content);

        totalPages = pageResult.getTotalPages();
        totalElements = pageResult.getTotalElements();
    }

    private Specification<Doctor> buildSpecification(){
        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get(Doctor_.name)));
            if (name != null) predicates.add(criteriaBuilder.like(root.get(Doctor_.name), "%" + name + "%"));
            if (lastName != null) predicates.add(criteriaBuilder.like(root.get(Doctor_.lastName), "%" + lastName + "%"));

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
