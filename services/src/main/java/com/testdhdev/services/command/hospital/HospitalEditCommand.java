package com.testdhdev.services.command.hospital;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.hospital.HospitalEditInput;
import com.testdhdev.services.model.domain.Hospital;
import com.testdhdev.services.model.repository.HospitalRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class HospitalEditCommand implements BusinessLogicCommand {
    @Setter
    private HospitalEditInput hospitalEditInput;

    @Getter
    private Hospital hospital;

    @Autowired
    private HospitalRepository hospitalRepository;

    @Override
    public void execute(){
        hospital = editHospital(hospitalEditInput);
    }

    private Hospital editHospital(HospitalEditInput editInput){
        Hospital hospital = hospitalRepository.findHospitalById(editInput.getHospitalId());
        hospital.setUpdateBy(editInput.getUser());
        hospital.setUpdatedDate(new Date());
        hospital.setName(editInput.getName());

        return hospitalRepository.save(hospital);
    }
}
