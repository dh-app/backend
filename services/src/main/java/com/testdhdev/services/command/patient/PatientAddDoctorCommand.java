package com.testdhdev.services.command.patient;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.patient.PatientAddDoctorsInput;
import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.domain.Patient;
import com.testdhdev.services.model.repository.DoctorRepository;
import com.testdhdev.services.model.repository.PatientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class PatientAddDoctorCommand implements BusinessLogicCommand {
    @Setter
    private PatientAddDoctorsInput patientAddDoctorsInput;

    @Getter
    private Patient patient;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Override
    public void execute(){
        patient = addPatientToDoctor(patientAddDoctorsInput);
    }

    private Patient addPatientToDoctor(PatientAddDoctorsInput input){
        Patient patient = patientRepository.findPatientById(input.getPatientId());
        input.getDoctors().forEach(doctorId -> {
            Doctor doctor = doctorRepository.findDoctorById(doctorId);
            patient.addDoctor(doctor);
        });

        return patientRepository.save(patient);
    }
}
