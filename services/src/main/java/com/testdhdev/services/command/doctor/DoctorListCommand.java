package com.testdhdev.services.command.doctor;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.repository.DoctorRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class DoctorListCommand implements BusinessLogicCommand {
    @Getter
    private List<Doctor> doctors;

    @Autowired
    private DoctorRepository doctorRepository;

    @Override
    public void execute(){
        doctors = doctorRepository.findAll();
    }
}
