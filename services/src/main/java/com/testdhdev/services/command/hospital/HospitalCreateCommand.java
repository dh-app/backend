package com.testdhdev.services.command.hospital;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.hospital.HospitalCreateInput;
import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.domain.Hospital;
import com.testdhdev.services.model.domain.Patient;
import com.testdhdev.services.model.repository.DoctorRepository;
import com.testdhdev.services.model.repository.HospitalRepository;
import com.testdhdev.services.model.repository.PatientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class HospitalCreateCommand implements BusinessLogicCommand {

    @Setter
    private HospitalCreateInput createInput;

    @Getter
    private Hospital hospital;

    @Autowired
    private HospitalRepository hospitalRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Override
    public void execute(){
        hospital = createHospital(createInput);
    }

    private Hospital createHospital(HospitalCreateInput hospitalCreateInput){
        Hospital hospital = composeHospital(hospitalCreateInput);

        return hospitalRepository.save(hospital);
    }

    private Hospital composeHospital(HospitalCreateInput hospitalCreateInput){
        Hospital hospital = new Hospital();
        hospital.setCreatedBy(hospitalCreateInput.getUser());
        hospital.setName(hospitalCreateInput.getName());
        hospital.setUpdateBy(hospitalCreateInput.getUser());
        hospitalCreateInput.getPatients().forEach(patientId -> {
            Patient patient = patientRepository.findPatientById(patientId);
            hospital.addPatient(patient);
        });
        hospitalCreateInput.getDoctors().forEach(doctorId -> {
            Doctor doctor = doctorRepository.findDoctorById(doctorId);
            hospital.addDoctor(doctor);
        });

        return hospital;
    }
}
