package com.testdhdev.services.command.patient;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.model.domain.Patient;
import com.testdhdev.services.model.domain.Patient_;
import com.testdhdev.services.model.repository.PatientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class PatientSearchCommand implements BusinessLogicCommand {
    @Setter
    private Integer limit;

    @Setter
    private Integer page;

    @Setter
    private String name;

    @Setter
    private String lastName;

    @Getter
    private List<Patient> patients;

    @Getter
    private Long totalElements;

    @Getter
    private Integer totalPages;

    @Autowired
    private PatientRepository patientRepository;

    @Override
    public void execute(){
        patients = new ArrayList<>();

        PageRequest pageRequest = PageRequest.of(page, limit);
        Page<Patient> pageResult = patientRepository.findAll(buildSpecification(), pageRequest);
        List<Patient> content = pageResult.getContent();

        if (!CollectionUtils.isEmpty(content)) patients.addAll(content);

        totalPages = pageResult.getTotalPages();
        totalElements = pageResult.getTotalElements();
    }

    private Specification<Patient> buildSpecification(){
        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get(Patient_.name)));
            if (name != null) predicates.add(criteriaBuilder.like(root.get(Patient_.name), "%" + name + "%"));
            if (lastName != null) predicates.add(criteriaBuilder.like(root.get(Patient_.lastName), "%" + lastName + "%"));

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
