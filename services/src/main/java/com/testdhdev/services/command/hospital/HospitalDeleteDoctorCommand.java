package com.testdhdev.services.command.hospital;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.hospital.HospitalDeleteDoctorInput;
import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.domain.Hospital;
import com.testdhdev.services.model.repository.DoctorRepository;
import com.testdhdev.services.model.repository.HospitalRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class HospitalDeleteDoctorCommand implements BusinessLogicCommand {
    @Setter
    private HospitalDeleteDoctorInput deleteDoctorInput;

    @Getter
    private Hospital hospital;

    @Autowired
    private HospitalRepository hospitalRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Override
    public void execute(){
        hospital = deleteDoctor(deleteDoctorInput);
    }

    private Hospital deleteDoctor(HospitalDeleteDoctorInput input){
        Hospital hospital = hospitalRepository.findHospitalById(input.getHospitalId());
        Doctor doctor = doctorRepository.findDoctorById(input.getDoctorId());
        hospital.removeDoctor(doctor);

        return hospitalRepository.save(hospital);
    }
}
