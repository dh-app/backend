package com.testdhdev.services.command.patient;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.patient.PatientAddNotesInput;
import com.testdhdev.services.model.domain.Note;
import com.testdhdev.services.model.domain.Patient;
import com.testdhdev.services.model.repository.PatientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class PatientAddNoteCommand implements BusinessLogicCommand {

    @Setter
    private PatientAddNotesInput addNotesInput;

    @Autowired
    private PatientRepository patientRepository;

    @Getter
    private Patient patient;

    @Override
    public void execute(){
        patient = addNotesToPatient(addNotesInput);
    }

    private Patient addNotesToPatient(PatientAddNotesInput addNotesInput){
        Patient instance = patientRepository.findPatientById(addNotesInput.getPatientId());
        for(Note note: addNotesInput.getNotes()){
            instance.addNote(note);
        }

        return patientRepository.save(instance);
    }
}
