package com.testdhdev.services.command.hospital;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.hospital.HospitalAddPatientInput;
import com.testdhdev.services.model.domain.Hospital;
import com.testdhdev.services.model.domain.Patient;
import com.testdhdev.services.model.repository.HospitalRepository;
import com.testdhdev.services.model.repository.PatientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class HospitalAddPatientCommand implements BusinessLogicCommand {
    @Setter
    private HospitalAddPatientInput patientInput;

    @Getter
    private Hospital hospital;

    @Autowired
    private HospitalRepository hospitalRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Override
    public void execute(){
        hospital = addPatientHospital(patientInput);
    }

    private Hospital addPatientHospital(HospitalAddPatientInput input){
        Hospital hospital = hospitalRepository.findHospitalById(input.getHospitalId());
        input.getPatients().forEach(id -> {
            Patient patient = patientRepository.findPatientById(id);
            hospital.addPatient(patient);
        });

        return hospitalRepository.save(hospital);
    }
}
