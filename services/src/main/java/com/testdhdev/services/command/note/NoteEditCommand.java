package com.testdhdev.services.command.note;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.note.NoteEditInput;
import com.testdhdev.services.model.domain.Note;
import com.testdhdev.services.model.repository.NoteRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class NoteEditCommand implements BusinessLogicCommand {
    @Setter
    private NoteEditInput noteEditInput;

    @Getter
    private Note note;

    @Autowired
    private NoteRepository noteRepository;

    @Override
    public void execute(){
        note = editNote(noteEditInput);
    }

    private Note editNote(NoteEditInput input){
        Note note = noteRepository.findNoteById(input.getNoteId());
        note.setUpdatedDate(new Date());
        note.setDescription(input.getDescription());
        note.setUpdateBy(input.getUser());

        return noteRepository.save(note);
    }
}
