package com.testdhdev.services.command.doctor;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.doctor.DoctorCreateInput;
import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.domain.Speciality;
import com.testdhdev.services.model.repository.DoctorRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class DoctorCreateCommand implements BusinessLogicCommand {
    @Setter
    private DoctorCreateInput doctorCreateInput;

    @Getter
    private Doctor doctor;

    @Autowired
    private DoctorRepository doctorRepository;

    @Override
    public void execute(){
        doctor = saveDoctor(doctorCreateInput);
    }

    private Doctor saveDoctor(DoctorCreateInput doctorCreateInput){
        Doctor doctor = composeDoctor(doctorCreateInput);

        return doctorRepository.save(doctor);
    }

    private Doctor composeDoctor(DoctorCreateInput doctorCreateInput){
        Doctor doctor = new Doctor();
        doctor.setName(doctorCreateInput.getName());
        doctor.setLastName(doctorCreateInput.getLastName());
        doctor.setDateBirth(doctorCreateInput.getDateBirth());
        doctor.setAddress(doctorCreateInput.getAddress());
        doctor.setProfileImageUrl(doctorCreateInput.getProfileImageUrl());
        doctor.setCreatedBy(doctorCreateInput.getUser());
        doctor.setUpdateBy(doctorCreateInput.getUser());
        for (Speciality speciality: doctorCreateInput.getSpecialities()){
            doctor.addSpeciality(speciality);
        }

        return doctor;
    }
}
