package com.testdhdev.services.command.doctor;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.doctor.DoctorAddSpecialityInput;
import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.domain.Speciality;
import com.testdhdev.services.model.repository.DoctorRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class DoctorAddSpecialityCommand implements BusinessLogicCommand {
    @Getter
    private Doctor doctor;

    @Setter
    private DoctorAddSpecialityInput input;

    @Autowired
    private DoctorRepository doctorRepository;

    @Override
    public void execute(){
        doctor = composeDoctorSpeciality(input);
    }

    private Doctor composeDoctorSpeciality(DoctorAddSpecialityInput input){
        Doctor doctor = doctorRepository.findDoctorById(input.getDoctorId());
        for (Speciality speciality: input.getSpecialities()){
            doctor.addSpeciality(speciality);
        }
        return doctorRepository.save(doctor);
    }
}
