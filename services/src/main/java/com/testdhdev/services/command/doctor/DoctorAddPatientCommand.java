package com.testdhdev.services.command.doctor;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.doctor.DoctorAddPatientInput;
import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.domain.Patient;
import com.testdhdev.services.model.repository.DoctorRepository;
import com.testdhdev.services.model.repository.PatientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class DoctorAddPatientCommand implements BusinessLogicCommand {

    @Setter
    private DoctorAddPatientInput addPatientInput;

    @Getter
    private Doctor doctor;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Override
    public void execute(){
        doctor = addPatientDoctor(addPatientInput);
    }

    private Doctor addPatientDoctor(DoctorAddPatientInput input){
        Doctor doctor = doctorRepository.findDoctorById(input.getDoctorId());
        input.getPatients().forEach(id -> {
            Patient patient = patientRepository.findPatientById(id);
            doctor.addPatient(patient);
        });

        return doctorRepository.save(doctor);
    }
}
