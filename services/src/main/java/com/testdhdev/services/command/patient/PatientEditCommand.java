package com.testdhdev.services.command.patient;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.patient.PatientEditInput;
import com.testdhdev.services.model.domain.Patient;
import com.testdhdev.services.model.repository.PatientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class PatientEditCommand implements BusinessLogicCommand {

    @Getter
    private Patient patient;

    @Setter
    private PatientEditInput input;

    @Autowired
    private PatientRepository patientRepository;

    @Override
    public void execute(){
        patient = editPatient(input);
    }

    private Patient editPatient(PatientEditInput input){
        Patient patient = patientRepository.findPatientById(input.getPatientId());
        patient.setDateBirth(input.getDateBirth());
        patient.setAddress(input.getAddress());
        patient.setLastName(input.getLastName());
        patient.setName(input.getName());
        patient.setProfileImageUrl(input.getName());
        patient.setUpdateBy(input.getUser());
        patient.setUpdatedDate(new Date());

        return patientRepository.save(patient);
    }
}
