package com.testdhdev.services.command.hospital;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.model.domain.Hospital;
import com.testdhdev.services.model.domain.Hospital_;
import com.testdhdev.services.model.repository.HospitalRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;
import javax.persistence.criteria.Predicate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class HospitalSearchCommand implements BusinessLogicCommand {
    @Setter
    private Integer limit;

    @Setter
    private Integer page;

    @Setter
    private String name;

    @Getter
    private List<Hospital> hospitals;

    @Getter
    private Long totalElements;

    @Getter
    private Integer totalPages;

    @Autowired
    private HospitalRepository hospitalRepository;

    @Override
    public void execute(){
        hospitals = new ArrayList<>();

        PageRequest pageRequest = PageRequest.of(page, limit);
        Page<Hospital> pageResult = hospitalRepository.findAll(buildSpecification(), pageRequest);
        List<Hospital> content = pageResult.getContent();

        if (!CollectionUtils.isEmpty(content)) hospitals.addAll(content);

        totalPages = pageResult.getTotalPages();
        totalElements = pageResult.getTotalElements();
    }

    private Specification<Hospital> buildSpecification(){
        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get(Hospital_.name)));
            if (name != null) predicates.add(criteriaBuilder.like(root.get(Hospital_.name), "%" + name + "%"));

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
