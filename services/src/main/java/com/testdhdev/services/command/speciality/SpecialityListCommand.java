package com.testdhdev.services.command.speciality;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.model.domain.Speciality;
import com.testdhdev.services.model.repository.SpecialityRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class SpecialityListCommand implements BusinessLogicCommand {
    @Getter
    private List<Speciality> specialities;

    @Autowired
    private SpecialityRepository specialityRepository;

    @Override
    public void execute(){
        specialities = specialityRepository.findAll();
    }
}
