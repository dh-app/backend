package com.testdhdev.services.command.note;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.model.domain.Note;
import com.testdhdev.services.model.repository.NoteRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class NoteDeleteCommand implements BusinessLogicCommand {
    @Setter
    private Long noteId;

    @Getter
    private List<Note> notes;

    @Autowired
    private NoteRepository noteRepository;

    @Override
    public void execute(){
        notes = deleteNote(noteId);
    }

    private List<Note> deleteNote(Long noteId){
        noteRepository.deleteById(noteId);

        return noteRepository.findAll();
    }
}
