package com.testdhdev.services.command.speciality;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.speciality.SpecialityEditInput;
import com.testdhdev.services.model.domain.Speciality;
import com.testdhdev.services.model.repository.SpecialityRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class SpecialityEditCommand implements BusinessLogicCommand {
    @Setter
    private SpecialityEditInput editInput;

    @Getter
    private Speciality speciality;

    @Autowired
    private SpecialityRepository specialityRepository;

    @Override
    public void execute(){
        speciality = editSpeciality(editInput);
    }

    private Speciality editSpeciality(SpecialityEditInput input){
        Speciality speciality = specialityRepository.findSpecialityById(input.getSpecialityId());
        speciality.setUpdateBy(input.getUser());
        speciality.setName(input.getName());
        speciality.setUpdatedDate(new Date());
        speciality.setDescription(input.getDescription());

        return specialityRepository.save(speciality);
    }
}
