package com.testdhdev.services.command.speciality;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.model.domain.Speciality;
import com.testdhdev.services.model.repository.SpecialityRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class SpecialityDeleteCommand implements BusinessLogicCommand {
    @Setter
    private Long specialityId;

    @Autowired
    private SpecialityRepository specialityRepository;

    @Getter
    private List<Speciality> specialities;

    @Override
    public void execute(){
        specialities = deleteSpeciality(specialityId);
    }

    private List<Speciality> deleteSpeciality(Long id){
        specialityRepository.deleteById(id);

        return specialityRepository.findAll();
    }
}
