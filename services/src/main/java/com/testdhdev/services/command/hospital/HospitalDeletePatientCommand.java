package com.testdhdev.services.command.hospital;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.testdhdev.services.input.hospital.HospitalDeletePatientInput;
import com.testdhdev.services.model.domain.Hospital;
import com.testdhdev.services.model.domain.Patient;
import com.testdhdev.services.model.repository.HospitalRepository;
import com.testdhdev.services.model.repository.PatientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class HospitalDeletePatientCommand implements BusinessLogicCommand {

    @Setter
    private HospitalDeletePatientInput patientInput;

    @Getter
    private Hospital hospital;

    @Autowired
    private HospitalRepository hospitalRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Override
    public void execute(){
        hospital = deletePatientHospital(patientInput);
    }

    private Hospital deletePatientHospital(HospitalDeletePatientInput input){
        Hospital hospital = hospitalRepository.findHospitalById(input.getHospitalId());
        Patient patient = patientRepository.findPatientById(input.getPatientId());
        hospital.removePatient(patient);

        return hospitalRepository.save(hospital);
    }
}
