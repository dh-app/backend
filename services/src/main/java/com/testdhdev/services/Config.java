package com.testdhdev.services;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Jhonatan Candia Romero
 */
@EnableAutoConfiguration
@Configuration
@ComponentScan("com.testdhdev.services")
public class Config {
}
