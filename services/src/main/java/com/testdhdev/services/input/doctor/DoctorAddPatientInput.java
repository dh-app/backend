package com.testdhdev.services.input.doctor;

import lombok.Getter;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class DoctorAddPatientInput {
    private Long doctorId;
    private List<Long> patients;
}
