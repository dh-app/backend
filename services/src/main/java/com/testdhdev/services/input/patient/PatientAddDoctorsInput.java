package com.testdhdev.services.input.patient;

import lombok.Getter;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class PatientAddDoctorsInput {
    private Long patientId;
    private List<Long> doctors;
}
