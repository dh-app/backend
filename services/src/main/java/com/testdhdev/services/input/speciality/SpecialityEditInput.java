package com.testdhdev.services.input.speciality;

import lombok.Getter;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class SpecialityEditInput {
    private Long specialityId;
    private String name;
    private String description;
    private String user;
}
