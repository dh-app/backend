package com.testdhdev.services.input.hospital;

import lombok.Getter;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class HospitalAddDoctorInput {
    private Long hospitalId;
    private List<Long> doctors;
}
