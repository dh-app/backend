package com.testdhdev.services.input.hospital;

import lombok.Getter;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class HospitalEditInput {
    private Long hospitalId;
    private String name;
    private String user;
}
