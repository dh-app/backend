package com.testdhdev.services.input.hospital;

import lombok.Getter;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class HospitalAddPatientInput {
    private Long hospitalId;
    private List<Long> patients;
}
