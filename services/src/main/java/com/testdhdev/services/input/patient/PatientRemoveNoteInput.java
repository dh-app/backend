package com.testdhdev.services.input.patient;

import lombok.Getter;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class PatientRemoveNoteInput {
    private Long noteId;
    private Long patientId;
}
