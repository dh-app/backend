package com.testdhdev.services.input.hospital;

import lombok.Getter;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class HospitalDeletePatientInput {
    private Long hospitalId;
    private Long patientId;
}
