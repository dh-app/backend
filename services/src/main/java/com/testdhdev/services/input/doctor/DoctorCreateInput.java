package com.testdhdev.services.input.doctor;

import com.testdhdev.services.model.domain.Speciality;
import lombok.Getter;

import java.util.Date;
import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class DoctorCreateInput {
    private String name;
    private String lastName;
    private Date dateBirth;
    private String address;
    private String profileImageUrl;
    private String user;
    private List<Speciality> specialities;
}

