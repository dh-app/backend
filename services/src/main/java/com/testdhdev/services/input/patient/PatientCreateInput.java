package com.testdhdev.services.input.patient;

import lombok.Getter;

import java.util.Date;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class PatientCreateInput {
    private String name;
    private String lastName;
    private Date dateBirth;
    private String address;
    private String profileImageUrl;
    private String user;
}
