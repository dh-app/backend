package com.testdhdev.services.input.patient;

import com.testdhdev.services.model.domain.Note;
import lombok.Getter;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class PatientAddNotesInput {
    private Long patientId;
    private List<Note> notes;
}
