package com.testdhdev.services.input.hospital;

import lombok.Getter;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class HospitalDeleteDoctorInput {
    private Long hospitalId;
    private Long doctorId;
}
