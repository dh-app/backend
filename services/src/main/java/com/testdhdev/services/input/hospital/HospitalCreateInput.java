package com.testdhdev.services.input.hospital;

import lombok.Getter;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class HospitalCreateInput {
    private String name;
    private List<Long> patients;
    private List<Long> doctors;
    private String user;
}
