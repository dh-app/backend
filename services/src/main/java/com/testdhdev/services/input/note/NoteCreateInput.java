package com.testdhdev.services.input.note;

import lombok.Getter;

import java.util.Date;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class NoteCreateInput {
    private String description;
    private Date createdDate;
    private String user;
}
