package com.testdhdev.services.input.note;

import lombok.Getter;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class NoteEditInput {
    private Long noteId;
    private String user;
    private String description;
}
