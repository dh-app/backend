package com.testdhdev.services.input.doctor;

import lombok.Getter;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class DoctorRemoveSpecialityInput {
    private Long doctorId;
    private Long specialityId;
}
