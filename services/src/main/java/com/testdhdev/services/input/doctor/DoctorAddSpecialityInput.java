package com.testdhdev.services.input.doctor;

import com.testdhdev.services.model.domain.Speciality;
import lombok.Getter;

import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class DoctorAddSpecialityInput {
    private Long doctorId;
    private List<Speciality> specialities;
}
