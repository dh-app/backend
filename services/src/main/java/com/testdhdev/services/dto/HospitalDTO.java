package com.testdhdev.services.dto;

import com.testdhdev.services.model.domain.Hospital;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class HospitalDTO implements Serializable {
    private Long id;
    private String name;
    private List<PDoctorDTO> doctors;
    private List<DPatientsDTO> patients;

    public HospitalDTO(Hospital hospital){
        this.id = hospital.getId();
        this.name = hospital.getName();
        this.doctors = hospital.getDoctors()
                .stream()
                .map(doctor -> new PDoctorDTO(doctor)).collect(Collectors.toList());
        this.patients = hospital.getPatients()
                .stream()
                .map(patient -> new DPatientsDTO(patient)).collect(Collectors.toList());
    }
}
