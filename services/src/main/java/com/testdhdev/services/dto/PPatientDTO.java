package com.testdhdev.services.dto;

import com.testdhdev.services.model.domain.Note;
import com.testdhdev.services.model.domain.Patient;
import lombok.Getter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class PPatientDTO implements Serializable {
    private Long id;
    private String name;
    private String lastName;
    private Date dateBirth;
    private String address;
    private String profileImageUrl;
    private List<Note> notes;
    private List<PDoctorDTO> doctors;

    public PPatientDTO(Patient patient){
        this.id = patient.getId();
        this.name = patient.getName();
        this.lastName = patient.getLastName();
        this.dateBirth = patient.getDateBirth();
        this.address = patient.getAddress();
        this.profileImageUrl = patient.getProfileImageUrl();
        this.notes = patient.getNotes();
        this.doctors = patient.getDoctors()
                .stream()
                .map(doctor -> new PDoctorDTO(doctor)).collect(Collectors.toList());
    }
}
