package com.testdhdev.services.dto;

import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.domain.Speciality;
import lombok.Getter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class DDoctorDTO implements Serializable {
    private Long id;
    private String name;
    private String lastName;
    private Date dateBirth;
    private String address;
    private String profileImageUrl;
    private List<Speciality> specialities;
    private List<DPatientsDTO> patients;

    public DDoctorDTO(Doctor doctor){
        this.id = doctor.getId();
        this.name = doctor.getName();
        this.lastName = doctor.getLastName();
        this.dateBirth = doctor.getDateBirth();
        this.address = doctor.getAddress();
        this.profileImageUrl = doctor.getProfileImageUrl();
        this.specialities = doctor.getSpecialities();
        this.patients = doctor.getPatients()
                .stream()
                .map(patient -> new DPatientsDTO(patient)).collect(Collectors.toList());
    }
}
