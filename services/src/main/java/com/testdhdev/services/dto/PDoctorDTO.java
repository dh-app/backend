package com.testdhdev.services.dto;

import com.testdhdev.services.model.domain.Doctor;
import com.testdhdev.services.model.domain.Speciality;
import lombok.Getter;

import java.util.Date;
import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class PDoctorDTO {
    private Long id;
    private String name;
    private String lastName;
    private Date dateBirth;
    private String address;
    private String profileImageUrl;
    private List<Speciality> specialities;

    public PDoctorDTO(Doctor doctor){
        this.id = doctor.getId();
        this.name = doctor.getName();
        this.lastName = doctor.getLastName();
        this.dateBirth = doctor.getDateBirth();
        this.address = doctor.getAddress();
        this.profileImageUrl = doctor.getProfileImageUrl();
        this.specialities = doctor.getSpecialities();
    }
}
