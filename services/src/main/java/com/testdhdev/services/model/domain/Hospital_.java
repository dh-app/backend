package com.testdhdev.services.model.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * @author Jhonatan Candia Romero
 */
@StaticMetamodel(Hospital.class)
public class Hospital_ {
    public static volatile SingularAttribute<Hospital, String> name;
    public static volatile SingularAttribute<Hospital, Date> createdDate;
}
