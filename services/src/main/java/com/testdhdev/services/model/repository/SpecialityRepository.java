package com.testdhdev.services.model.repository;

import com.testdhdev.services.model.domain.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Jhonatan Candia Romero
 */
public interface SpecialityRepository extends JpaRepository<Speciality, Long> {
    @Query("select speciality from Speciality speciality where speciality.id = :id")
    Speciality findSpecialityById(@Param("id") Long id);
}
