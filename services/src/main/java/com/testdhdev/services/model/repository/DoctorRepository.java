package com.testdhdev.services.model.repository;

import com.testdhdev.services.model.domain.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Jhonatan Candia Romero
 */
public interface DoctorRepository extends JpaRepository<Doctor, Long>, JpaSpecificationExecutor<Doctor> {
    @Query("select doctor from Doctor doctor where doctor.id = :id")
    Doctor findDoctorById(@Param("id") Long id);
}
