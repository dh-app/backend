package com.testdhdev.services.model.repository;

import com.testdhdev.services.model.domain.Hospital;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Jhonatan Candia Romero
 */
public interface HospitalRepository extends JpaRepository<Hospital, Long>, JpaSpecificationExecutor<Hospital> {
    @Query("select hospital from Hospital hospital where hospital.id = :id")
    Hospital findHospitalById(@Param("id") Long id);
}
