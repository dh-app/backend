package com.testdhdev.services.model.repository;

import com.testdhdev.services.model.domain.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Jhonatan Candia Romero
 */
public interface PatientRepository extends JpaRepository<Patient, Long>, JpaSpecificationExecutor<Patient> {
    @Query("select patient from Patient patient where patient.id = :id")
    Patient findPatientById(@Param("id") Long id);
}
