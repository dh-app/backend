package com.testdhdev.services.model.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * @author Jhonatan Candia Romero
 */
@StaticMetamodel(Patient_.class)
public class Patient_ {
    public static volatile SingularAttribute<Patient, String> name;
    public static volatile SingularAttribute<Patient, String> lastName;
    public static volatile SingularAttribute<Patient, Date> dateBirth;
}
