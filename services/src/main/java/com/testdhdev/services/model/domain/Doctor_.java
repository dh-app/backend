package com.testdhdev.services.model.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * @author Jhonatan Candia Romero
 */
@StaticMetamodel(Doctor.class)
public class Doctor_ {
    public static volatile SingularAttribute<Doctor, String> name;
    public static volatile SingularAttribute<Doctor, String> lastName;
    public static volatile SingularAttribute<Doctor, String> dateBirth;
}
