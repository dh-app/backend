package com.testdhdev.services.model.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Jhonatan Candia Romero
 */
@Entity
@Data
@Table(name = "notes")
public class Note implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "noteid", nullable = false)
    private Long id;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false, updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date updatedDate;

    @Column(nullable = false, updatable = false)
    private String createdBy;

    @Column(nullable = false)
    private String updateBy;

    @Override
    public boolean equals(Object object){
        if (this == object) return true;
        if (!(object instanceof Note)) return false;
        Note note = (Note) object;

        return this.id != null && this.id.equals(note.getId());
    }
}
