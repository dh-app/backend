package com.testdhdev.services.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@Entity
@Data
@Table(name = "patients")
public class Patient implements Serializable {
    @Id
    @Column(name = "patientid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String lastName;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date dateBirth;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private String profileImageUrl;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Note> notes;

    @JsonIgnore
    @ManyToMany(mappedBy = "patients")
    private List<Hospital> hospitals;

    @ManyToMany(mappedBy = "patients")
    private List<Doctor> doctors;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date updatedDate;

    @Column(nullable = false, updatable = false)
    private String createdBy;

    @Column(nullable = false)
    private String updateBy;

    public Patient(){
        this.doctors = new ArrayList<>();
        this.notes = new ArrayList<>();
    }

    @PrePersist
    public void onPrePersist(){
        this.createdDate = new Date();
        this.updatedDate = new Date();
    }

    @Override
    public boolean equals(Object object){
        if (this == object) return true;
        if (!(object instanceof Patient)) return false;
        Patient patient = (Patient) object;

        return this.id != null && this.id.equals(patient.getId());
    }

    public void addDoctor(Doctor doctor){
        this.doctors.add(doctor);
    }

    public void removeDoctor(Doctor doctor){
        this.doctors.remove(doctor);
    }

    public void addNote(Note note){
        this.notes.add(note);
    }

    public void removeNote(Note note){
        this.notes.remove(note);
    }
}
