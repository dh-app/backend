package com.testdhdev.services.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@Entity
@Data
@Table(name = "doctors")
public class Doctor implements Serializable {
    @Id
    @Column(name = "doctorid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String lastName;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date dateBirth;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private String profileImageUrl;

    @JoinTable(
            name = "doctors_specialities",
            joinColumns = @JoinColumn(name = "fk_doc", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "fk_spec", nullable = false)
    )
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Speciality> specialities;

    @JsonIgnore
    @ManyToMany(mappedBy = "doctors")
    private List<Hospital> hospitals;

    @JoinTable(
            name = "doctors_patients",
            joinColumns = @JoinColumn(name = "fk_doc", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "fk_patients", nullable = false)
    )
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Patient> patients;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date updatedDate;

    @Column(nullable = false, updatable = false)
    private String createdBy;

    @Column(nullable = false)
    private String updateBy;

    public Doctor(){
        this.patients = new ArrayList<>();
        this.specialities = new ArrayList<>();
    }

    @PrePersist
    public void onPrePersist(){
        this.createdDate = new Date();
        this.updatedDate = new Date();
    }

    @Override
    public boolean equals(Object object){
        if (this == object) return true;
        if (!(object instanceof Doctor)) return false;
        Doctor doctor = (Doctor) object;

        return this.id != null && this.id.equals(doctor.getId());
    }

    public void addPatient(Patient patient){
        this.patients.add(patient);
    }

    public void removePatient(Patient patient){
        this.patients.remove(patient);
    }

    public void addSpeciality(Speciality speciality){
        this.specialities.add(speciality);
    }

    public void removeSpeciality(Speciality speciality){
        this.specialities.remove(speciality);
    }
}
