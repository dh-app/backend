package com.testdhdev.services.model.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Jhonatan Candia Romero
 */
@Entity
@Data
@Table(name = "hospitals")
public class Hospital implements Serializable {

    @Id
    @Column(name = "hospitalid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false)
    private String name;

    @JoinTable(
            name = "hospital_doctors",
            joinColumns = @JoinColumn(name = "fk_hospital", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "fk_doctors", nullable = false)
    )
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Doctor> doctors;

    @JoinTable(
            name = "hospital_patients",
            joinColumns = @JoinColumn(name = "fk_hospital", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "fk_patients")
    )
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Patient> patients;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date updatedDate;

    @Column(nullable = false, updatable = false)
    private String createdBy;

    @Column(nullable = false)
    private String updateBy;

    public Hospital(){
        this.doctors = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    @PrePersist
    public void onPrePersist(){
        this.createdDate = new Date();
        this.updatedDate = new Date();
    }

    public void addDoctor(Doctor doctor){
        this.doctors.add(doctor);
    }

    public void addPatient(Patient patient){
        this.patients.add(patient);
    }

    public void removeDoctor(Doctor doctor){
        this.doctors.remove(doctor);
    }

    public void removePatient(Patient patient){
        this.patients.remove(patient);
    }
}
