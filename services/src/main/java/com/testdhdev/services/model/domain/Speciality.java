package com.testdhdev.services.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Jhonatan Candia Romero
 */
@Entity
@Data
@Table(name = "specialities", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")
})
public class Speciality implements Serializable {
    @Id
    @Column(name = "specialityid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @JsonIgnore
    @ManyToMany(mappedBy = "specialities")
    private List<Doctor> doctors;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date updatedDate;

    @Column(nullable = false, updatable = false)
    private String createdBy;

    @Column(nullable = false)
    private String updateBy;

    @PrePersist
    public void onPrePersist(){
        this.createdDate = new Date();
        this.updatedDate = new Date();
    }

    @Override
    public boolean equals(Object object){
        if (this == object) return true;
        if (!(object instanceof Speciality)) return false;
        Speciality speciality = (Speciality) object;

        return this.id != null && this.id.equals(speciality.getId());
    }
}
