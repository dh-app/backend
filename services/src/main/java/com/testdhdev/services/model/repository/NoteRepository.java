package com.testdhdev.services.model.repository;

import com.testdhdev.services.model.domain.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Jhonatan Candia Romero
 */
public interface NoteRepository extends JpaRepository<Note, Long> {

    @Query("select note from Note note where note.id = :id")
    Note findNoteById(@Param("id") Long id);
}
